//
//  Video.swift
//  HLS
//
//  Created by gilbert.floarea on 7/27/20.
//  Copyright © 2020 gilbert.floarea. All rights reserved.
//

import UIKit

class Video: NSObject {
  let url: URL
  let thumbURL: URL
  let title: String
  let subtitle: String

  init(url: URL, thumbURL: URL, title: String, subtitle: String) {
    self.url = url
    self.thumbURL = thumbURL
    self.title = title
    self.subtitle = subtitle
    super.init()
  }
  
  class func localVideos() -> [Video] {
    var videos: [Video] = []
    let names = ["newYorkFlip", "bulletTrain", "monkey", "shark"]
    let titles = ["New York Flip", "Bullet Train Adventure", "Monkey Village", "Robot Battles"]
    let subtitles = ["Can this guys really flip all of his bros? You'll never believe what happens!",
                     "Enjoying the soothing view of passing towns in Japan",
                     "Watch as a roving gang of monkeys terrorizes the top of this mountain!",
                     "Have you ever seen a robot shark try to eat another robot?"]
    
    for (index, name) in names.enumerated() {
      let urlPath = Bundle.main.path(forResource: name, ofType: "mp4")!
      let url = URL(fileURLWithPath: urlPath)
      let thumbURLPath = Bundle.main.path(forResource: name, ofType: "png")!
      let thumbURL = URL(fileURLWithPath: thumbURLPath)
      
      let video = Video(url: url, thumbURL: thumbURL, title: titles[index], subtitle: subtitles[index])
      videos.append(video)
    }
    return videos
  }
  
  class func allVideos() -> [Video] {
    var videos = localVideos()
    
    // MARK: -  Add remote videos
    let videoURLString = "https://wolverine.raywenderlich.com/content/ios/tutorials/video_streaming/foxVillage.m3u8"
    if let url = URL(string: videoURLString) {
      let thumbURLPath = Bundle.main.path(forResource: "foxVillage", ofType: "png")!
      let thumbURL = URL(fileURLWithPath: thumbURLPath)
      let remoteVideo = Video(url: url, thumbURL: thumbURL, title: "キツネ村", subtitle: "Fox")
      videos.append(remoteVideo)
    }
    
    let video2URLString = "https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8"
    if let url = URL(string: video2URLString) {
      let thumbURLPath = Bundle.main.path(forResource: "123123", ofType: "png")!
      let thumbURL = URL(fileURLWithPath: thumbURLPath)
      let remoteVideo = Video(url: url, thumbURL: thumbURL, title: "Scenary", subtitle: "Hello!")
      videos.append(remoteVideo)
    }
    
    let video3URLString = "https://mnmedias.api.telequebec.tv/m3u8/29880.m3u8"
    if let url = URL(string: video3URLString) {
      let thumbURLPath = Bundle.main.path(forResource: "112233", ofType: "png")!
      let thumbURL = URL(fileURLWithPath: thumbURLPath)
      let remoteVideo = Video(url: url, thumbURL: thumbURL, title: "Crazy talks", subtitle: "17 mins video!")
      videos.append(remoteVideo)
    }
    
    let video4URLString = "https://cdn.api.video/vod/vi7YtQMo94w57RKbdk4gwX4Y/hls/manifest.m3u8"
    if let url = URL(string: video4URLString) {
      let thumbURLPath = Bundle.main.path(forResource: "thumbnail", ofType: "jpg")!
      let thumbURL = URL(fileURLWithPath: thumbURLPath)
      let remoteVideo = Video(url: url, thumbURL: thumbURL, title: "Washing machine", subtitle: "Crazy spinning!")
      videos.append(remoteVideo)
    }
    
    return videos
  }
}
