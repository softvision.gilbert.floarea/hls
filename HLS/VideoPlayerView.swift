//
//  VideoPlayerView.swift
//  HLS
//
//  Created by gilbert.floarea on 7/27/20.
//  Copyright © 2020 gilbert.floarea. All rights reserved.
//

import UIKit
import AVFoundation

class VideoPlayerView: UIView {
    
    // MARK: -  Add player property so the video the layer shows can be updated
    var player: AVPlayer? {
        get {
            return playerLayer.player
        }
        
        set {
            playerLayer.player = newValue
        }
    }
    
    // MARK: -  Override the layerClass
    override class var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
    
    // MARK: -  Add accessor for playerLayer so you don't need to
    var playerLayer: AVPlayerLayer {
        return layer as! AVPlayerLayer
    }
}
